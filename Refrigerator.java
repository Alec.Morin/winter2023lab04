public class Refrigerator{
	
	private boolean freezer;
	
	private int temperature;
	
	private int noShelf;
	
	public Refrigerator(int newTemp){
		this.freezer = true;
		this.temperature = newTemp;
		this.noShelf = 0;
	}
	
	
	public void ifFreezer(){
		if (freezer){
			System.out.println("Good for you! You have a freezer!!");
		}
		else{
			System.out.println("Go buy a fridge with a freezer!!!");
		}
	}

	public void checkTemperature(){
		System.out.println("The current temperature is " + temperature);
	}
	
	public void remShelf(int noRemoved){
		if (verifyShelf(noRemoved))
		this.noShelf = this.noShelf - noRemoved;
	}
	
	private boolean verifyShelf(int noRemoved){
		if (this.noShelf > noRemoved && this.noShelf > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public boolean getFreezer(){
		return this.freezer;
	}
	
	public int getTemp(){
		return this.temperature;
	}
	
	public int getNoShelf(){
		return this.noShelf;
	}
	
	
	public void setFreezer(boolean newFreezer){
		this.freezer = newFreezer;
	}
	
	public void setNoShelf(int newNo){
		this.noShelf = newNo;
	}
}