import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[]args){
		Scanner scan = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		
		for (int i = 0; i < appliances.length; i++){
			appliances[i] = new Refrigerator(-60);
			boolean x = scan.nextBoolean();
			appliances[i].setFreezer(x); 
			int z = scan.nextInt();
			appliances[i].setNoShelf(z);
			
			System.out.println();
		}
		
		System.out.println(appliances[3].getFreezer() + ", " + appliances[3].getTemp() + ", "+appliances[3].getNoShelf());
		boolean k = scan.nextBoolean();
		appliances[3].setFreezer(k);
		int g = scan.nextInt();
		appliances[3].setNoShelf(g);
		System.out.println(appliances[3].getFreezer() + ", " + appliances[3].getTemp() + ", "+appliances[3].getNoShelf());
		
		appliances[0].ifFreezer();
		appliances[0].checkTemperature();
		
		appliances[1].remShelf(1);
		System.out.println("The new total shelves are " + appliances[1].getNoShelf());
	}
}